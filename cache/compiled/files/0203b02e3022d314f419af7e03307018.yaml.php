<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/var/www/examples/grav-skeleton-receptar-site/user/config/plugins/simplesearch.yaml',
    'modified' => 1531521775,
    'data' => [
        'route' => '/search',
        'template' => 'simplesearch_results',
        'filters' => NULL,
        'order' => [
            'by' => 'date',
            'dir' => 'desc'
        ]
    ]
];
